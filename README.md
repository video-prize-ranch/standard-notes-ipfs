# standard-notes-ipfs
Standard Notes extensions on IPFS! You do not need an Extended subscription to use these.

> Some IPFS gateways may not work due to CORS

## Extensions

### Task Editor
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/task-editor/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/QmY8wqRAGSaabALHGM3PVnekGTafymdDBeedt3RjoFi3HC

### Fast Editor
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/fast-editor/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/QmbXVRX1ayMkwYQtMARUqessdDVTd9SproQcS7mUEZtw85

### Markdown Pro
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/markdown-pro/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/Qmd97izo2DA3AK1cvz7aZPgqMqieJ48cTbDR9tFqTGBTuA

### Secure Spreadsheets Pro
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/secure-spreadsheets/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/QmViPQNZDmzCMosorAHurXHQ12htEvLX8L7n2iccQEWkfH

### Markdown Visual
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/markdown-visual/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/QmWqNa9xMfcV5h6HWdoiphzr4XVWZAPtRdkbZJvX2WTs57