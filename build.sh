#!/usr/bin/env bash

source .env
source ~/.bashrc

# Build Task Editor
mkdir -p build/task-editor
cd build/task-editor
cp ../../files/task-editor/ext.json .
cp ../../files/task-editor/ext.pages.json .

wget -O dist.zip https://github.com/standardnotes/simple-task-editor/releases/download/1.3.10/org.standardnotes.simple-task-editor.zip
DOWNLOAD_CID=$(ipfs add -qnr dist.zip)
sed -i "s/SED_REPLACE_URL2/$DOWNLOAD_CID/g" ext.json

unzip dist.zip -d dist

DIST_CID=$(ipfs add -Qnr dist)
sed -i "s/SED_REPLACE_URL/$DIST_CID/g" ext.json

echo " "
echo "Task Editor built with URL:"
EXT1_CID=$(ipfs add -qnr ext.json)
echo "https://ipfs.io/ipfs/${EXT1_CID}"
echo " "
cd ../..

# Build Fast Editor
mkdir -p build/fast-editor
cd build/fast-editor
cp ../../files/fast-editor/ext.json .
cp ../../files/fast-editor/ext.pages.json .

wget -O dist.zip https://github.com/dec0dOS/standard-notes-fast-editor/archive/4.0.2.zip
unzip dist.zip
mv standard-notes-fast-editor-4.0.2 dist

rm dist.zip
zip -r dist.zip dist
DOWNLOAD_CID=$(ipfs add -qnr dist.zip)
sed -i "s/SED_REPLACE_URL2/$DOWNLOAD_CID/g" ext.json

DIST_CID=$(ipfs add -Qnr dist)
sed -i "s/SED_REPLACE_URL/$DIST_CID/g" ext.json

rm -rf src

echo " "
echo "Fast Editor built with URL:"
EXT2_CID=$(ipfs add -qnr ext.json)
echo "https://ipfs.io/ipfs/${EXT2_CID}"
echo " "
cd ../..

# Build Markdown Pro
mkdir -p build/markdown-pro
cd build/markdown-pro
cp ../../files/markdown-pro/ext.json .
cp ../../files/markdown-pro/ext.pages.json .

wget -O dist.zip https://github.com/standardnotes/markdown-pro/releases/download/1.5.0/org.standardnotes.advanced-markdown-editor.zip
DOWNLOAD_CID=$(ipfs add -qnr dist.zip)
sed -i "s/SED_REPLACE_URL2/$DOWNLOAD_CID/g" ext.json

unzip dist.zip -d dist

DIST_CID=$(ipfs add -Qnr dist)
sed -i "s/SED_REPLACE_URL/$DIST_CID/g" ext.json

echo " "
echo "Markdown Pro built with URL:"
EXT3_CID=$(ipfs add -qnr ext.json)
echo "https://ipfs.io/ipfs/${EXT3_CID}"
echo " "
cd ../..

# Build Secure Spreadsheets Pro
mkdir -p build/secure-spreadsheets
cd build/secure-spreadsheets
cp ../../files/secure-spreadsheets/ext.json .
cp ../../files/secure-spreadsheets/ext.pages.json .

wget -O dist.zip https://github.com/standardnotes/secure-spreadsheets/releases/download/1.4.4/org.standardnotes.standard-sheets.zip
DOWNLOAD_CID=$(ipfs add -qnr dist.zip)
sed -i "s/SED_REPLACE_URL2/$DOWNLOAD_CID/g" ext.json

unzip dist.zip -d dist

DIST_CID=$(ipfs add -Qnr dist)
sed -i "s/SED_REPLACE_URL/$DIST_CID/g" ext.json

echo " "
echo "Secure Spreadsheets Pro built with URL:"
EXT4_CID=$(ipfs add -qnr ext.json)
echo "https://ipfs.io/ipfs/${EXT4_CID}"
echo " "
cd ../..

# Build Markdown Visual
mkdir -p build/markdown-visual
cd build/markdown-visual
cp ../../files/markdown-visual/ext.json .
cp ../../files/markdown-visual/ext.pages.json .

wget -O dist.zip https://github.com/standardnotes/markdown-visual/releases/download/1.0.6/org.standardnotes.markdown-visual-editor.zip
DOWNLOAD_CID=$(ipfs add -qnr dist.zip)
sed -i "s/SED_REPLACE_URL2/$DOWNLOAD_CID/g" ext.json

unzip dist.zip -d dist

DIST_CID=$(ipfs add -Qnr dist)
sed -i "s/SED_REPLACE_URL/$DIST_CID/g" ext.json

echo " "
echo "Markdown Visual built with URL:"
EXT5_CID=$(ipfs add -qnr ext.json)
echo "https://ipfs.io/ipfs/${EXT5_CID}"
echo " "
cd ../..

rm README.md
cp README.template.md README.md
sed -i "s/EXT_CID1/$EXT1_CID/g" README.md
sed -i "s/EXT_CID2/$EXT2_CID/g" README.md
sed -i "s/EXT_CID3/$EXT3_CID/g" README.md
sed -i "s/EXT_CID4/$EXT4_CID/g" README.md
sed -i "s/EXT_CID5/$EXT5_CID/g" README.md

git add .
git commit -m "Updated extension"

read -p "Press Enter to continue" </dev/tty

# Deploy to Codeberg Pages
mv build /tmp/build-aq21dxa
mv .env ../
mv .fleek.json ../
git branch -D pages
git switch --orphan pages
rm -rf *
mv /tmp/build-aq21dxa/* .
rm -rf /tmp/build-aq21dxa
git add .
git commit -m "Update site"
git push --force --set-upstream origin pages
git checkout main
mv ../.env .
mv ../.fleek.json .