# standard-notes-ipfs
Standard Notes extensions on IPFS! You do not need an Extended subscription to use these.

> Some IPFS gateways may not work due to CORS

## Extensions

### Task Editor
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/task-editor/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/EXT_CID1

### Fast Editor
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/fast-editor/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/EXT_CID2

### Markdown Pro
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/markdown-pro/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/EXT_CID3

### Secure Spreadsheets Pro
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/secure-spreadsheets/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/EXT_CID4

### Markdown Visual
Always up to date URL: https://raw.codeberg.page/video-prize-ranch/standard-notes-ipfs/@pages/markdown-visual/ext.json

Extension URL: https://gateway.pinata.cloud/ipfs/EXT_CID5